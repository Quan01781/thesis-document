'use strict';
const db = require("../models");
const CareSite = db.CareSite;
const Provider = db.Provider;
const VisitOccurrence = db.VisitOccurrence;
const { Op } = require("sequelize");
const ConceptService = require("./concept");
const Measurement = db.Measurement;
const Concept = db.Concept;


exports.getTotalDoctor = async (parentCareSiteId) => {
    try {

        let careSiteList = await CareSite.findAll({where:{parent_care_site_id:parentCareSiteId}});

        let totalDoctor = [];
        for(let x in careSiteList) {
            totalDoctor.push(await Provider.count({
                    where: {
                        provider_role_concept_id: 8,
                        care_site_id: careSiteList[x].care_site_id,
                    }})
            )}

        let result = [];
        for(let i = 0; i<totalDoctor.length; i++){
            result.push({
                ["care_site_id"]:careSiteList[i].care_site_id,
                ["care_site_name"]:careSiteList[i].care_site_name,
                ["total_doctor"]:totalDoctor[i]
            });}

        return { isSuccess: true, data: result };
    }
    catch (error) {
        return { isSuccess: false, message: error };
    }
};

exports.getTotalStaff = async (parentCareSiteId) => {
    try {
        let careSiteList = await CareSite.findAll({where:{parent_care_site_id:parentCareSiteId}});

        let totalStaff = [];
        for(let x in careSiteList) {
            totalStaff.push(await Provider.count({
                where: {
                    care_site_id: careSiteList[x].care_site_id,
                }})
            )}

        let result = [];
        for(let i = 0; i<totalStaff.length; i++){
            result.push({
                ["care_site_id"]:careSiteList[i].care_site_id,
                ["care_site_name"]:careSiteList[i].care_site_name,
                ["total_staff"]:totalStaff[i]
            });}

        return { isSuccess: true, data: result };
    }
    catch (error) {
        return { isSuccess: false, message: error };
    }
};

exports.getTotalVisitInDay = async (parentCareSiteId) => {
    try {
        let careSiteList = await CareSite.findAll({where:{parent_care_site_id:parentCareSiteId}});

        const TODAY_START = new Date().setHours(0, 0, 0, 0);
        const NOW = new Date();

        let totalVisit = [];
        for(let x in careSiteList) {
            totalVisit.push(await VisitOccurrence.count({
                where: {
                    visit_start_datetime: {
                        [Op.between]: [TODAY_START, NOW]
                    },
                    care_site_id: careSiteList[x].care_site_id,
                }})
            )}

        let result = [];
        for(let i = 0; i<totalVisit.length; i++){
            result.push({
                ["care_site_id"]:careSiteList[i].care_site_id,
                ["care_site_name"]:careSiteList[i].care_site_name,
                ["total_visit"]:totalVisit[i]
            });}

        return { isSuccess: true, data: result };
    }
    catch (error) {
        return { isSuccess: false, message: error };
    }
};

exports.getTotalVisitByTime = async (parentCareSiteId,start,end)=>{
    try{
        let careSiteList = await CareSite.findAll({where:{parent_care_site_id:parentCareSiteId}});

        let totalVisit = [];
        for(let x in careSiteList) {
            totalVisit.push(await VisitOccurrence.count({
                where: {
                    visit_start_datetime: {[Op.gte]: start},
                    visit_end_datetime: {[Op.lte]: end},
                    care_site_id: careSiteList[x].care_site_id,
                }})
            )}

        let result = [];
        for(let i = 0; i<totalVisit.length; i++){
            result.push({
                ["care_site_id"]:careSiteList[i].care_site_id,
                ["care_site_name"]:careSiteList[i].care_site_name,
                ["total_visit"]:totalVisit[i]
            });}

        return { isSuccess: true, data: result };
    }
    catch (error){
        return { isSuccess: false, message: error };
    }

}

exports.getTotalMeasurement = async (domainId, parentCareSiteId,start,end) => {
    try {
        const level1Concept = await ConceptService.getLevel1ConceptMeasurement(domainId, parentCareSiteId, null);
        if (!level1Concept) {
            return [false, 'Cannot Get Level 1 Concept Measurement'];
        }

        let listMeasurementNameLv2 = await Concept.findAll({
                    attributes:['concept_id', 'concept_name'],
                     include:[{
                        attributes:[],
                        model: Concept,
                        as:'ancestors',
                        where:{
                        concept_id: level1Concept.concept_id,
                        }}]
                })

        let listMeasurementNameLv3 = [];
        for (let x in listMeasurementNameLv2){
                 listMeasurementNameLv3.push(await Concept.findAll({
                 attributes:['concept_id','concept_name'],
                 include:[{
                     attributes:[],
                     model: Concept,
                     as:'ancestors',
                     where:{
                     concept_id: listMeasurementNameLv2[x].concept_id
                     }
                 }]
                 }))}
        let listMeasurementNameLv4 = [];
        for (let x in listMeasurementNameLv3){
            for(let y =0;y<listMeasurementNameLv3[x].length;y++){
            listMeasurementNameLv4.push(await Concept.findAll({
                attributes: ['concept_name'],
                include:[{
                    attributes:[],
                    model: Concept,
                    as:'ancestors',
                    where:{
                        concept_id: listMeasurementNameLv3[x][y].concept_id
                    }
                }]
            }))}}
        /*total measurement Lv4*/
        let countLv4 =[];
        for(let x in listMeasurementNameLv3){
            for(let y =0;y<listMeasurementNameLv3[x].length;y++){
                countLv4.push(await Measurement.count({
                    group:'measurement_concept_id',
                    where:{measurement_datetime: {
                        [Op.between]: [start, end]
                    }},
                    include:[{
                        model:Concept,
                        as:'measurement_concept',
                        required:true,
                        include:[{
                            model:Concept,
                            as:'ancestors',
                            required:true,
                            where:{
                                concept_id:listMeasurementNameLv3[x][y].concept_id,
                            }}]}]}))
            }
        }
        let tempLv4 =[];
        for(let x =0; x<listMeasurementNameLv4.length;x++){
            if(listMeasurementNameLv4[x].length !== 0){
            for (let y = 0; y < listMeasurementNameLv4[x].length; y++) {
                tempLv4.push({
                    ["Measurement_name"]:listMeasurementNameLv4[x][y].concept_name,
                    ["total"]:countLv4[x][y].count
                })
            }
        }else{tempLv4.push([])}
        }
        let totalLv4 = listMeasurementNameLv4;
        for(let x =0; x<totalLv4.length;x++){
                for (let y = 0; y < totalLv4[x].length; y++) {
                   totalLv4[x][y] = tempLv4[0];
                   tempLv4 = tempLv4.splice(1);
                }
            }
        /*total measurement Lv3*/
        let countMeasurementLv3 = [];
        for(let x in listMeasurementNameLv3){
                for(let y = 0;y<listMeasurementNameLv3[x].length; y++ ){
                    countMeasurementLv3.push(await Measurement.count({
                        where:{
                            measurement_datetime: {
                                [Op.between]: [start, end]
                            },
                            measurement_concept_id: listMeasurementNameLv3[x][y].concept_id
                        }
                    }))}
        }

        let tempLv3 =[];
        let copyTotalLv4 = totalLv4;
        for(let x =0; x<listMeasurementNameLv3.length;x++){
                for (let y = 0; y < listMeasurementNameLv3[x].length; y++) {
                    tempLv3.push({
                        ["Measurement_name"]:listMeasurementNameLv3[x][y].concept_name,
                        ["total"]:countMeasurementLv3[0],
                        ["descendants"]: copyTotalLv4[0],
                    })
                    countMeasurementLv3 = countMeasurementLv3.splice(1);
                    copyTotalLv4 = copyTotalLv4.splice(1)
                }

        }
        let totalLv3 = listMeasurementNameLv3;
        for(let x in totalLv3){
                for(let y = 0;y<totalLv3[x].length; y++ ){
                    totalLv3[x][y] = tempLv3[0];
                    tempLv3 = tempLv3.splice(1)
                }
        }
        // /* total Measurement Lv2*/
        let countLv2 = 0;
        let sumLv2 =[]
        for(let x in totalLv3){
            for(let y = 0;y<totalLv3[x].length; y++ ){
                countLv2 = countLv2 + parseInt(totalLv3[x][y].total)
            }
            sumLv2.push(countLv2);
            countLv2 =0;
        }
        let totalLv2 =[]
        for(let x in listMeasurementNameLv2){
            totalLv2.push({
                ["Measurement_name"]:listMeasurementNameLv2[x].concept_name,
                ["total"]:sumLv2[x],
                ["descendants"]:totalLv3[x]
            })
        }
        /*total Measurement Lv1*/
        let total =0;
        for(let x in sumLv2){total = total + sumLv2[x]}
        let result ={
            name:level1Concept.concept_name,
            total:total,
            descendants:totalLv2,
        };
        return { isSuccess: true, data: result };
    }
    catch (error){
        return { isSuccess: false, message: error };
    }
}


exports.getTotalProfit = async (domainId, parentCareSiteId,start,end) => {
    try {
        let careSiteList = await CareSite.findAll({where:{parent_care_site_id:parentCareSiteId}});
        let parentCareSite = await CareSite.findAll({where:{care_site_id:parentCareSiteId}})

        const level1Concept = await ConceptService.getLevel1ConceptMeasurement(domainId, parentCareSiteId, null);
        if (!level1Concept) {
            return [false, 'Cannot Get Level 1 Concept Measurement'];
        }
        let listMeasurementNameLv2 = await Concept.findAll({
            attributes:['concept_id', 'concept_name'],
            include:[{
                attributes:[],
                model: Concept,
                as:'ancestors',
                where:{
                    concept_id: level1Concept.concept_id,
                }}]
        })

        let listMeasurementNameLv3 = [];
        for (let x in listMeasurementNameLv2){
            listMeasurementNameLv3.push(await Concept.findAll({
                attributes:['concept_id','concept_name'],
                include:[{
                    attributes:[],
                    model: Concept,
                    as:'ancestors',
                    where:{
                        concept_id: listMeasurementNameLv2[x].concept_id
                    }
                }]
            }))}

        let totalProfit = [];
        for(let a in careSiteList){
            let listProvider = await Provider.findAll({where:{care_site_id:careSiteList[a].care_site_id},
                include:[{
                    attributes:['concept_name'],
                    model: Concept,
                    as:'role_concept',
                    where:{concept_id:{[Op.or]:[8,9]}}
                }]})
        let list =[]
        for(let x in listMeasurementNameLv3){
            for(let y in listMeasurementNameLv3[x]){
                for(let z =0;z<listProvider.length;z++){
                    let sum =await Measurement.findAll({
                        attributes:['price_value'],
                        where:{
                            measurement_datetime: {[Op.between]: [start, end]},
                            provider_id:listProvider[z].provider_id,
                            measurement_concept_id:listMeasurementNameLv3[x][y].concept_id}
                    })
                    if(sum.length !== 0){list.push(sum[0])}
                }}}
        let profit = 0;
        for(let x in list){profit = profit + list[x].price_value}
        totalProfit.push(profit)}
        /*doanh thu từng phòng khám*/
        let listCareSiteProfit =[]
        for(let x in careSiteList){
            listCareSiteProfit.push({
                ["care_site_name"]:careSiteList[x].care_site_name,
                ["Profit"]:totalProfit[x]
                })
        }
        /*doanh thu cả chuỗi*/
        let finalProfit =0
        for(let x = 0;x<totalProfit.length;x++){
            finalProfit = finalProfit + totalProfit[x];
        }
        let result ={
            parent_care_site_name:parentCareSite[0].care_site_name,
            profit:finalProfit,
            descendants:listCareSiteProfit
        }
        return { isSuccess: true, data: result };
    }
    catch (error){
        return { isSuccess: false, message: error };
    }
}

exports.getProfitEachMeasurement = async (domainId, parentCareSiteId,start,end) => {
    try {
        const level1Concept = await ConceptService.getLevel1ConceptMeasurement(domainId, parentCareSiteId, null);
        if (!level1Concept) {
            return [false, 'Cannot Get Level 1 Concept Measurement'];
        }

        let listMeasurementNameLv2 = await Concept.findAll({
            attributes:['concept_id', 'concept_name'],
            include:[{
                attributes:[],
                model: Concept,
                as:'ancestors',
                where:{
                    concept_id: level1Concept.concept_id,
                }}]
        })

        let listMeasurementNameLv3 = [];
        for (let x in listMeasurementNameLv2){
            listMeasurementNameLv3.push(await Concept.findAll({
                attributes:['concept_id','concept_name'],
                include:[{
                    attributes:[],
                    model: Concept,
                    as:'ancestors',
                    where:{
                        concept_id: listMeasurementNameLv2[x].concept_id
                    }
                }]
            }))}

        let sumProfit =[]
        for(let x in listMeasurementNameLv3){
            for(let y = 0; y<listMeasurementNameLv3[x].length;y++){
                sumProfit.push(await Measurement.sum('price_value',{
                    where:{
                        measurement_concept_id:listMeasurementNameLv3[x][y].concept_id,
                        measurement_datetime: {[Op.between]: [start, end]}
                    }}))
            }
        }
        let profitMeasurementLv3 = listMeasurementNameLv3
        let temp =[]
        for(let x in listMeasurementNameLv3){
            for(let y = 0; y<listMeasurementNameLv3[x].length;y++){
                temp.push({
                    ["measurement_Name"]:listMeasurementNameLv3[x][y].concept_name,
                    ["profit"]:sumProfit[0]
                });
                sumProfit = sumProfit.splice(1);
            }
        }
        for(let x in profitMeasurementLv3){
            for(let y = 0; y<profitMeasurementLv3[x].length;y++){
                profitMeasurementLv3[x][y] = temp[0];
                temp = temp.splice(1);
            }
        }
        /*Profit for Lv2*/
        let sumProfitLv2 = 0;
        let total =[];
        for(let x in profitMeasurementLv3){
            for(let y =0;y<profitMeasurementLv3[x].length;y++){
                sumProfitLv2 = sumProfitLv2 + profitMeasurementLv3[x][y].profit
            }
            total.push(sumProfitLv2);
            sumProfitLv2 = 0
        }

        let result =[]
        for(let x in listMeasurementNameLv2){
            result.push({
                ["measurement_name"]:listMeasurementNameLv2[x].concept_name,
                ["profit"]:total[x],
                ["descendants"]:profitMeasurementLv3[x]
            })
        }

        return { isSuccess: true, data: result };
    }
    catch (error){
        return { isSuccess: false, message: error };
    }
}
