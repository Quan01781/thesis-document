const ConceptService = require("./concept");
const { Op } = require("sequelize");
const db = require("../models");
const moment = require("moment");
const Concept = db.Concept;
const Measurement = db.Measurement;
const VisitOccurrence = db.VisitOccurrence;
const CareSite = db.CareSite;
const Provider = db.Provider;
const MeasurementPrice = db.MeasurementPrice;
const Person = db.Person;

exports.getProfitMeasurement = async (domainId, parentCareSiteId, start, end,CareSiteId) => {
    try {
        const level1Concept = await ConceptService.getLevel1ConceptMeasurement(domainId, parentCareSiteId, null);
        if (!level1Concept) {
            return [false, 'Cannot Get Level 1 Concept Measurement'];
        }

        let listMeasurementNameLv2 = await Concept.findAll({
            attributes:['concept_id', 'concept_name'],
            include:[{
                attributes:[],
                model: Concept,
                as:'ancestors',
                where:{
                    concept_id: level1Concept.concept_id,
                }}]
        })

        let listMeasurementNameLv3 = [];
        for (let x in listMeasurementNameLv2){
            listMeasurementNameLv3.push(await Concept.findAll({
                attributes:['concept_id','concept_name'],
                include:[{
                    attributes:[],
                    model: Concept,
                    as:'ancestors',
                    where:{
                        concept_id: listMeasurementNameLv2[x].concept_id
                    }
                }]
            }))}

        let listProvider = await Provider.findAll({where:{care_site_id:CareSiteId},
            include:[{
                attributes:['concept_name'],
                model: Concept,
                as:'role_concept',
                where:{concept_id:{[Op.or]:[8,9]}}
            }]})

        let listProfit =[]
        let test =[]
            for(let x in listMeasurementNameLv3){
                for(let y = 0; y<listMeasurementNameLv3[x].length;y++){
                    for(let z = 0; z<listProvider.length;z++){
                    test.push(await Measurement.sum('price_value',{
                    where:{measurement_concept_id:listMeasurementNameLv3[x][y].concept_id,
                        provider_id:listProvider[z].provider_id,
                        measurement_datetime: {[Op.between]: [start, end]}}}))

            }
                    listProfit.push(test);
                    test = test.splice(listProvider.length)
        }}
            /*sum profit of each measurement*/
            let sumProfitLv3 = [];
            let count =0;
            for(let x in listProfit){
                for(let y = 0;  y<listProfit[x].length;y++){
                    count = count + listProfit[x][y];
                }
                sumProfitLv3.push(count);
                count =0;
            }
        let profitMeasurementLv3 = listMeasurementNameLv3;
        let temp =[];
        for(let x in listMeasurementNameLv3){
            for(let y = 0; y<listMeasurementNameLv3[x].length;y++){
                temp.push({
                    ["measurement_Name"]:listMeasurementNameLv3[x][y].concept_name,
                    ["profit"]:sumProfitLv3[0]
                });
                sumProfitLv3 = sumProfitLv3.splice(1);
            }
        }
        for(let x in profitMeasurementLv3){
            for(let y = 0; y<profitMeasurementLv3[x].length;y++){
                profitMeasurementLv3[x][y] = temp[0];
                temp = temp.splice(1);
            }
        }
        /*Profit for Lv2*/
        let sumProfitLv2 = 0;
        let totalLv2 =[];
        for(let x in profitMeasurementLv3){
            for(let y =0;y<profitMeasurementLv3[x].length;y++){
                sumProfitLv2 = sumProfitLv2 + profitMeasurementLv3[x][y].profit
            }
            totalLv2.push(sumProfitLv2);
            sumProfitLv2 = 0
        }

        let profitMeasurementLv2 =[]
        for(let x in listMeasurementNameLv2){
            profitMeasurementLv2.push({
                ["measurement_name"]:listMeasurementNameLv2[x].concept_name,
                ["profit"]:totalLv2[x],
                ["descendants"]:profitMeasurementLv3[x]
            })
        }
        /*total Profit*/
        let total = 0;
        for(let x in profitMeasurementLv2){
            total = total + parseFloat(profitMeasurementLv2[x].profit);
        }
        let careSiteName = await CareSite.findAll({where:{care_site_id:CareSiteId}})
        let result ={
            care_site_name:careSiteName[0].care_site_name,
            total_profit:total,
            profit_measurements:profitMeasurementLv2
        }
        return { isSuccess: true, data: result };
    }
    catch (error){
        return { isSuccess: false, message: error };
    }
}

exports.getTotalMeasurementDescendantCareSite = async (domainId, parentCareSiteId, start, end,CareSiteId)=>{
    try {
        const level1Concept = await ConceptService.getLevel1ConceptMeasurement(domainId, parentCareSiteId, null);
        if (!level1Concept) {
            return [false, 'Cannot Get Level 1 Concept Measurement'];
        }

        let listProvider = await Provider.findAll({where:{care_site_id:CareSiteId},
            include:[{
                attributes:['concept_name'],
                model: Concept,
                as:'role_concept',
                where:{concept_id:{[Op.or]:[8,9]}}
            }]})

        let listMeasurementNameLv2 = await Concept.findAll({
            attributes:['concept_id', 'concept_name'],
            include:[{
                attributes:[],
                model: Concept,
                as:'ancestors',
                where:{
                    concept_id: level1Concept.concept_id,
                }}]
        })

        let listMeasurementNameLv3 = [];
        for (let x in listMeasurementNameLv2){
            listMeasurementNameLv3.push(await Concept.findAll({
                attributes:['concept_id','concept_name'],
                include:[{
                    attributes:[],
                    model: Concept,
                    as:'ancestors',
                    where:{
                        concept_id: listMeasurementNameLv2[x].concept_id
                    }
                }]
            }))}
        let listMeasurementNameLv4 = [];
        for (let x in listMeasurementNameLv3){
            for(let y =0;y<listMeasurementNameLv3[x].length;y++){
                listMeasurementNameLv4.push(await Concept.findAll({
                    attributes: ['concept_name'],
                    include:[{
                        attributes:[],
                        model: Concept,
                        as:'ancestors',
                        where:{
                            concept_id: listMeasurementNameLv3[x][y].concept_id
                        }
                    }]
                }))}}

        let countLv4 =[];
        for(let x in listMeasurementNameLv3){
            for(let y =0;y<listMeasurementNameLv3[x].length;y++){
                for(let z =0; z<listProvider.length;z++){

                countLv4.push(await Measurement.count({
                    group:'measurement_concept_id',
                    where:{measurement_datetime: {
                            [Op.between]: [start, end]
                        },
                    provider_id:listProvider[z].provider_id
                    },
                    include:[{
                        model:Concept,
                        as:'measurement_concept',
                        required:true,
                        include:[{
                            model:Concept,
                            as:'ancestors',
                            required:true,
                            where:{
                                concept_id:listMeasurementNameLv3[x][y].concept_id,
                            }}]}]}))
            }}
        }
        let tempLv4 =[];
        for(let x =0; x<listMeasurementNameLv4.length;x++){
            if(listMeasurementNameLv4[x].length !== 0){
                for (let y = 0; y < listMeasurementNameLv4[x].length; y++) {
                    tempLv4.push({
                        ["Measurement_name"]:listMeasurementNameLv4[x][y].concept_name,
                        ["total"]:countLv4[x][y].count
                    })
                }
            }else{tempLv4.push([])}
        }
        let totalLv4 = listMeasurementNameLv4;
        for(let x =0; x<totalLv4.length;x++){
            for (let y = 0; y < totalLv4[x].length; y++) {
                totalLv4[x][y] = tempLv4[0];
                tempLv4 = tempLv4.splice(1);
            }
        }
        // /*total measurement Lv3*/
        let countMeasurementLv3 = [];
        for(let x in listMeasurementNameLv3){
            for(let y = 0;y<listMeasurementNameLv3[x].length; y++ ){
                for(let z =0; z<listProvider.length;z++){
                countMeasurementLv3.push(await Measurement.count({
                    where:{
                        measurement_datetime: {
                            [Op.between]: [start, end]
                        },
                        provider_id:listProvider[z].provider_id,
                        measurement_concept_id: listMeasurementNameLv3[x][y].concept_id
                    }
                }))}}
        }

        let tempLv3 =[];
        let copyTotalLv4 = totalLv4;
        for(let x =0; x<listMeasurementNameLv3.length;x++){
            for (let y = 0; y < listMeasurementNameLv3[x].length; y++) {
                tempLv3.push({
                    ["Measurement_name"]:listMeasurementNameLv3[x][y].concept_name,
                    ["total"]:countMeasurementLv3[0],
                    ["descendants"]: copyTotalLv4[0],
                })
                countMeasurementLv3 = countMeasurementLv3.splice(1);
                copyTotalLv4 = copyTotalLv4.splice(1)
            }
        }
        let totalLv3 = listMeasurementNameLv3;
        for(let x in totalLv3){
            for(let y = 0;y<totalLv3[x].length; y++ ){
                totalLv3[x][y] = tempLv3[0];
                tempLv3 = tempLv3.splice(1)
            }
        }
        // /* total Measurement Lv2*/
        let countLv2 = 0;
        let sumLv2 =[]
        for(let x in totalLv3){
            for(let y = 0;y<totalLv3[x].length; y++ ){
                countLv2 = countLv2 + parseInt(totalLv3[x][y].total)
            }
            sumLv2.push(countLv2);
            countLv2 =0;
        }
        let totalLv2 =[]
        for(let x in listMeasurementNameLv2){
            totalLv2.push({
                ["Measurement_name"]:listMeasurementNameLv2[x].concept_name,
                ["total"]:sumLv2[x],
                ["descendants"]:totalLv3[x]
            })
        }
        // /*total Measurement Lv1*/
        let total =0;
        for(let x in sumLv2){total = total + sumLv2[x]}
        let result ={
            name:level1Concept.concept_name,
            total:total,
            descendants:totalLv2,
        };
        return { isSuccess: true, data: result };
    }
    catch (error){
        return { isSuccess: false, message: error };
    }
}

exports.getTotalMeasurementForProvider = async (start, end,CareSiteId) => {
    try {
        let listProvider = await Provider.findAll({where:{care_site_id:CareSiteId},
        include:[{
            attributes:['concept_name'],
            model: Concept,
            as:'role_concept',
            where:{concept_id:{[Op.or]:[8,9]}}
        }]})

        let listMeasurement =[];
        for(let x in listProvider){
            listMeasurement.push(await Measurement.count({
                attributes:['measurement_concept_id'],
                group:['measurement_concept_id'],
                where:{[Op.or]:[{provider_id:listProvider[x].provider_id},{staff_id:listProvider[x].provider_id}],
                    measurement_datetime: {
                        [Op.between]: [start,end]
                    }},
            }))
        }

        let measurementName =[]
        for(let x in listMeasurement){
            for(let y =0;y<listMeasurement[x].length;y++){
                measurementName.push(await Concept.findAll({
                    attributes:['concept_name'],
                    where:{concept_id:listMeasurement[x][y].measurement_concept_id}
                }))
            }
        }

        let sum =0;
        let total =[];
        for(let x in listMeasurement){
            for(let y =0;y<listMeasurement[x].length;y++){
                sum = sum + listMeasurement[x][y].count
            }
            total.push(sum);
            sum =0;
        }

        for(let x in listMeasurement){
            for(let y =0;y<listMeasurement[x].length;y++){
                listMeasurement[x][y] = {
                    measurement_name:measurementName[0][0].concept_name,
                    count:listMeasurement[x][y].count,
                };
                measurementName = measurementName.splice(1)
            }
        }

        let result = [];
        for(let x in listProvider){
            result.push({
                provider_name:listProvider[x].provider_name,
                provider_id:listProvider[x].provider_id,
                provider_role:listProvider[x].role_concept.concept_name,
                count:total[x],
                detail:listMeasurement[x]
            })
        }
        return { isSuccess: true, data: result };
    }
    catch (error){
        return { isSuccess: false, message: error };
    }
}

exports.getCommissionForProvider = async (domainId, parentCareSiteId, start, end,CareSiteId) => {
    try {
        const level1Concept = await ConceptService.getLevel1ConceptMeasurement(domainId, parentCareSiteId, null);
        if (!level1Concept) {
            return [false, 'Cannot Get Level 1 Concept Measurement'];
        }

        let listMeasurementNameLv2 = await Concept.findAll({
            attributes:['concept_id', 'concept_name'],
            include:[{
                attributes:[],
                model: Concept,
                as:'ancestors',
                where:{
                    concept_id: level1Concept.concept_id,
                }}]
        })

        let listMeasurementNameLv3 = [];
        for (let x in listMeasurementNameLv2){
            listMeasurementNameLv3.push(await Concept.findAll({
                attributes:['concept_id','concept_name'],
                include:[{
                    attributes:[],
                    model: Concept,
                    as:'ancestors',
                    where:{
                        concept_id: listMeasurementNameLv2[x].concept_id
                    }
                }]
            }))}

        let listProvider = await Provider.findAll({where:{care_site_id:CareSiteId},
            include:[{
                attributes:['concept_name'],
                model: Concept,
                as:'role_concept',
                where:{concept_id:{[Op.or]:[8,9]}}
            }]})

        let countMeasurementLv3 = [];
        let temp =[]
        for(let x in listMeasurementNameLv3){
            for(let y = 0;y<listMeasurementNameLv3[x].length; y++ ){
                for(let z =0; z<listProvider.length;z++){
                    temp.push(await Measurement.count({
                        where:{
                            measurement_datetime: {
                                [Op.between]: [start, end]
                            },
                            provider_id:listProvider[z].provider_id,
                            measurement_concept_id: listMeasurementNameLv3[x][y].concept_id
                        }
                    }))}
                countMeasurementLv3.push(temp);
                temp = temp.splice(listProvider.length)
            }
        }
        /*count measurement*/
        let numberMeasurement = [];
        let count =0;
        for(let x in countMeasurementLv3){
            for(let y = 0;  y<countMeasurementLv3[x].length;y++){
                count = count + countMeasurementLv3[x][y];
            }
            numberMeasurement.push(count);
            count =0;
        }
        /*list commission*/
        let price;
        let listPrice = [];
        for(let x in listMeasurementNameLv3){
            for(let y = 0;y<listMeasurementNameLv3[x].length;y++){
                price = await MeasurementPrice.findAll({
                    attributes:['commission_value'],
                    where:{measurement_concept_id:listMeasurementNameLv3[x][y].concept_id,
                        descendant_care_site_id:CareSiteId}
                })
                if(price.length == 0){ price = await MeasurementPrice.findAll({
                    attributes:['commission_value'],
                    where:{measurement_concept_id:listMeasurementNameLv3[x][y].concept_id}
                })}
                listPrice.push(price[0])
            }
        }
        /*calculate commission each measurement*/
        for(let x = 0; x < numberMeasurement.length;x++){
            numberMeasurement[x] = numberMeasurement[x]*listPrice[x].commission_value
        }

        let commissionLv3 = listMeasurementNameLv3;
        for(let x in listMeasurementNameLv3){
            for(let y =0;y<listMeasurementNameLv3[x].length;y++){
                commissionLv3[x][y]={
                    measurement_name:listMeasurementNameLv3[x][y].concept_name,
                    commission:numberMeasurement[0]
                }
                numberMeasurement = numberMeasurement.splice(1)
            }
        }
        /*commission for Lv2*/
        let countLv2 =0
        let Lv2 =[]
        let commissionLv2 = listMeasurementNameLv2
        let total = 0
        for(let x in commissionLv3){
            for(let y =0;y<commissionLv3[x].length;y++){
                countLv2 = countLv2 + commissionLv3[x][y].commission;
            }
            Lv2.push(countLv2);
            total = total + countLv2;
            countLv2 = 0;
        }
        for(let x in listMeasurementNameLv2){
            commissionLv2[x] = {
                measurement_name:listMeasurementNameLv2[x].concept_name,
                commission:Lv2[x],
                descendants:commissionLv3[x]
            }
        }
        let careSite = await CareSite.findAll({where:{care_site_id:CareSiteId}})
        let result = {
            care_site_name: careSite[0].care_site_name,
            care_site_id: careSite[0].care_site_id,
            commission:total,
            descendants:commissionLv2
        }

        return { isSuccess: true, data: result };
    }
    catch (error){
        return { isSuccess: false, message: error };
    }
}

/*theo từng bệnh nhân*/
exports.getTotalVisitPatient = async (start, end, CareSiteId)=>{
    try{
        let visit =[];
        const FORMAT = 'YYYY-MM-DD hh:mm:ss';

        let listPatient = await VisitOccurrence.count({
            attributes:['person_id'],
            group:'person_id',
            where:{
                care_site_id: CareSiteId,
                visit_start_datetime: {[Op.gte]: start},
                visit_end_datetime: {[Op.lte]: end}
            }
            })

        for(let x in listPatient){
            visit.push(await VisitOccurrence.findAll({
                attributes:['visit_occurrence_id','visit_start_datetime','visit_end_datetime'],
                where:{
                    person_id:listPatient[x].person_id,
                    care_site_id: CareSiteId,
                    visit_start_datetime: {[Op.gte]: start},
                    visit_end_datetime: {[Op.lte]: end}
                },
                order:['visit_start_datetime']
            }))
        }

        let temp = visit;

        for(let y =0; y<visit.length;y++){
            for(let z =0; z<visit[y].length;z++){
                let a = new Date(visit[y][z].visit_start_datetime);
                let b = new Date(visit[y][z].visit_end_datetime);
                a.setHours(a.getHours() +7);
                b.setHours(b.getHours() +7);
                a = moment.utc(a,FORMAT).format("DD-MM-YYYY HH:mm:ss").replace(/T/, ' ').replace(/\..+/g, '');
                b = moment.utc(b,FORMAT).format("DD-MM-YYYY HH:mm:ss").replace(/T/, ' ').replace(/\..+/g, '');

                temp[y][z] = {
                    visit_occurrence_id: visit[y][z].visit_occurrence_id,
                    visit_start_datetime:a,
                    visit_end_datetime:b
                }
            }
        }

        let result = [];
        for(let x in listPatient){
            result.push({
                person_id: listPatient[x].person_id,
                all_visit:temp[x]
            })
        }

        return { isSuccess: true, data: result };
    }
    catch (error){
        return { isSuccess: false, message: error };
    }
}
