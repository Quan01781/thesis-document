const express = require('express');
const router = express.Router();
const Total = require('../controllers/statistics');

//thống kê theo từng chuỗi
router.get('/total-doctor', Total.listTotalDoctor);
router.get('/total-staff', Total.listTotalStaff);
router.get('/visitnow', Total.listTotalVisitInDay);
router.get('/visit', Total.listTotalVisitByTime);
router.get('/total-measurement', Total.listTotalMeasurement);
router.get('/profit', Total.listTotalProfit);
router.get('/profit-each-measurement', Total.listProfitEachMeasurement);

//theo từng phòng khám
router.get('/profit-measurement-caresite', Total.listProfitMeasurement);
router.get('/total-measurement-caresite', Total.listTotalMeasurementDescendantCareSite);
router.get('/provider-measurement', Total.listTotalMeasurementForProvider);
router.get('/commission-caresite', Total.listCommissionForProvider);

//theo từng bệnh nhân
router.get('/person-visit', Total.listTotalVisitPatient);

module.exports = router;
