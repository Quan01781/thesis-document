'use strict';
const StatParentCareSite = require('../services/stat_parent_care_site');
const StatCareSite = require('../services/stat_descendant_caresite');
const DomainService = require("../services/domain");
const CommonConst = require("../common/common_const");
const db = require("../models");
const CareSite = db.CareSite;

exports.listTotalDoctor = async (req, res) => {
    try {
        const parentCareSiteId = req.query.parent_care_site_id;
        if (!parentCareSiteId)
            return res.status(400).send("parent care site is required");

        let result = await StatParentCareSite.getTotalDoctor(parentCareSiteId);
        if (!result.isSuccess)
            return res.status(400).send('Cannot List Total Doctor.\n' + result.message);

        return res.status(200).send(result.data);
    }
    catch (error) {
        return res.status(400).json(error.name);
    }
};

exports.listTotalStaff = async (req, res) => {
    try {
        const parentCareSiteId = req.query.parent_care_site_id;
        if (!parentCareSiteId)
            return res.status(400).send("parent care site is required");

        let result = await StatParentCareSite.getTotalStaff(parentCareSiteId);
        if (!result.isSuccess)
            return res.status(400).send('Cannot List Total Staff.\n' + result.message);

        return res.status(200).send(result.data);
    }
    catch (error) {
        return res.status(400).json(error.name);
    }
};

exports.listTotalVisitInDay = async (req, res) => {
    try {
        const parentCareSiteId = req.query.parent_care_site_id;
        if (!parentCareSiteId)
            return res.status(400).send("parent care site is required");

        let result = await StatParentCareSite.getTotalVisitInDay(parentCareSiteId);
        if (!result.isSuccess)
            return res.status(400).send('Cannot List Total Visit.\n' + result.message);

        return res.status(200).send(result.data);
    }
    catch (error) {
        return res.status(400).json(error.name);
    }
};

exports.listTotalVisitByTime = async (req, res) => {
    try {
        const parentCareSiteId = req.query.parent_care_site_id;
        if (!parentCareSiteId)
            return res.status(400).send("parent care site is required");

        const START = new Date(req.query.start).setHours(0, 0, 0, 0);
        const END = new Date(req.query.end).setHours(23, 59, 59, 59);
        if(!START || !END)
            return res.status(400).send("time interval is missing");

        let result = await StatParentCareSite.getTotalVisitByTime(parentCareSiteId,START,END);
        if (!result.isSuccess)
            return res.status(400).send('Cannot List Total Visit.\n' + result.message);

        return res.status(200).send(result.data);
    }
    catch (error) {
        return res.status(400).json(error.name);
    }
};

exports.listTotalMeasurement = async (req, res) => {
    try {
        const parentCareSiteId = req.query.parent_care_site_id;
        if (!parentCareSiteId)
            return res.status(400).send("parent care site is required");

        // set time interval
        const START = new Date(req.query.start).setHours(0, 0, 0, 0);
        const END = new Date(req.query.end).setHours(23, 59, 59, 59);
        if(!START || !END)
            return res.status(400).send("time interval is missing");

        // Get domain: Measurement
        let domain = await DomainService.getDomainByName(CommonConst.DEFAULT_DOMAIN_NAME.MEASUREMENT);
        if (!domain)
            return res.status(400).send("Domain Measurement Not Found");

        let result = await StatParentCareSite.getTotalMeasurement(domain.domain_id, parentCareSiteId,START,END);
        if (!result.isSuccess)
            return res.status(400).send('Cannot List Total Measurement.\n' + result.message);

        return res.status(200).send(result.data);
    }
    catch (error) {
        return res.status(400).json(error.name);
    }
};

exports.listTotalProfit = async (req, res) => {
    try {
        const parentCareSiteId = req.query.parent_care_site_id;
        if (!parentCareSiteId)
            return res.status(400).send("parent care site is required");

        // set time interval
        const START = new Date(req.query.start).setHours(0, 0, 0, 0);
        const END = new Date(req.query.end).setHours(23, 59, 59, 59);
        if(!START || !END)
            return res.status(400).send("time interval is missing");

        // Get domain: Measurement
        let domain = await DomainService.getDomainByName(CommonConst.DEFAULT_DOMAIN_NAME.MEASUREMENT);
        if (!domain)
            return res.status(400).send("Domain Measurement Not Found");

        let result = await StatParentCareSite.getTotalProfit(domain.domain_id, parentCareSiteId,START,END);
        if (!result.isSuccess)
            return res.status(400).send('Cannot List Total Profit.\n' + result.message);

        return res.status(200).send(result.data);
    }
    catch (error) {
        return res.status(400).json(error.name);
    }
};

exports.listProfitEachMeasurement = async (req, res) => {
    try {
        const parentCareSiteId = req.query.parent_care_site_id;
        if (!parentCareSiteId)
            return res.status(400).send("parent care site is required");

        // set time interval
        const START = new Date(req.query.start).setHours(0, 0, 0, 0);
        const END = new Date(req.query.end).setHours(23, 59, 59, 59);
        if(!START || !END)
            return res.status(400).send("time interval is missing");

        // Get domain: Measurement
        let domain = await DomainService.getDomainByName(CommonConst.DEFAULT_DOMAIN_NAME.MEASUREMENT);
        if (!domain)
            return res.status(400).send("Domain Measurement Not Found");

        let result = await StatParentCareSite.getProfitEachMeasurement(domain.domain_id, parentCareSiteId,START,END);
        if (!result.isSuccess)
            return res.status(400).send('Cannot List Profit of Measurement.\n' + result.message);

        return res.status(200).send(result.data);
    }
    catch (error) {
        return res.status(400).json(error.name);
    }
};

/*thồng kê theo từng phòng khám*/
exports.listProfitMeasurement = async (req, res) => {
    try {

        const CareSiteId = req.query.care_site_id;
        if (!CareSiteId)
            return res.status(400).send("care site is required");

        let parentCareSiteId = await CareSite.findAll({
            attributes:['parent_care_site_id'],
            where:{care_site_id:req.query.care_site_id}});
        if (!parentCareSiteId)
            return res.status(400).send("wrong care site id");

        // set time interval
        const START = new Date(req.query.start).setHours(0, 0, 0, 0);
        const END = new Date(req.query.end).setHours(23, 59, 59, 59);
        if(!START || !END)
            return res.status(400).send("time interval is missing");

        // Get domain: Measurement
        let domain = await DomainService.getDomainByName(CommonConst.DEFAULT_DOMAIN_NAME.MEASUREMENT);
        if (!domain)
            return res.status(400).send("Domain Measurement Not Found");

        let result = await StatCareSite.getProfitMeasurement(domain.domain_id, parentCareSiteId[0].parent_care_site_id,START,END,CareSiteId);
        if (!result.isSuccess)
            return res.status(400).send('Cannot List Total Profit.\n' + result.message);

        return res.status(200).send(result.data);
    }
    catch (error) {
        return res.status(400).json(error.name);
    }
};

exports.listTotalMeasurementDescendantCareSite = async (req, res) => {
    try {

        const CareSiteId = req.query.care_site_id;
        if (!CareSiteId)
            return res.status(400).send("care site is required");

        let parentCareSiteId = await CareSite.findAll({
            attributes:['parent_care_site_id'],
            where:{care_site_id:req.query.care_site_id}});
        if (!parentCareSiteId)
            return res.status(400).send("wrong care site id");

        // set time interval
        const START = new Date(req.query.start).setHours(0, 0, 0, 0);
        const END = new Date(req.query.end).setHours(23, 59, 59, 59);
        if(!START || !END)
            return res.status(400).send("time interval is missing");

        // Get domain: Measurement
        let domain = await DomainService.getDomainByName(CommonConst.DEFAULT_DOMAIN_NAME.MEASUREMENT);
        if (!domain)
            return res.status(400).send("Domain Measurement Not Found");

        let result = await StatCareSite.getTotalMeasurementDescendantCareSite(domain.domain_id, parentCareSiteId[0].parent_care_site_id,START,END,CareSiteId);
        if (!result.isSuccess)
            return res.status(400).send('Cannot List Total Measurement.\n' + result.message);

        return res.status(200).send(result.data);
    }
    catch (error) {
        return res.status(400).json(error.name);
    }
};

exports.listTotalMeasurementForProvider = async (req, res) => {
    try {
        const CareSiteId = req.query.care_site_id;
        if (!CareSiteId)
            return res.status(400).send("care site is required");

        // set time interval
        const START = new Date(req.query.start).setHours(0, 0, 0, 0);
        const END = new Date(req.query.end).setHours(23, 59, 59, 59);
        if(!START || !END)
            return res.status(400).send("time interval is missing");

        let result = await StatCareSite.getTotalMeasurementForProvider(START,END,CareSiteId);
        if (!result.isSuccess)
            return res.status(400).send('Cannot List Total Measurement of doctor.\n' + result.message);

        return res.status(200).send(result.data);
    }
    catch (error) {
        return res.status(400).json(error.name);
    }
};

exports.listCommissionForProvider = async (req, res) => {
    try {
        const CareSiteId = req.query.care_site_id;
        if (!CareSiteId)
            return res.status(400).send("care site id is required");

        let parentCareSiteId = await CareSite.findAll({
            attributes:['parent_care_site_id'],
            where:{care_site_id:req.query.care_site_id}});
        if (!parentCareSiteId)
            return res.status(400).send("wrong care site id");

        // set time interval
        const START = new Date(req.query.start).setHours(0, 0, 0, 0);
        const END = new Date(req.query.end).setHours(23, 59, 59, 59);
        if(!START || !END)
            return res.status(400).send("time interval is missing");

        // Get domain: Measurement
        let domain = await DomainService.getDomainByName(CommonConst.DEFAULT_DOMAIN_NAME.MEASUREMENT);
        if (!domain)
            return res.status(400).send("Domain Measurement Not Found");

        let result = await StatCareSite.getCommissionForProvider(domain.domain_id, parentCareSiteId[0].parent_care_site_id, START,END,CareSiteId);
        if (!result.isSuccess)
            return res.status(400).send('Cannot List Commission.\n' + result.message);

        return res.status(200).send(result.data);
    }
    catch (error) {
        return res.status(400).json(error.name);
    }
};

exports.listTotalVisitPatient = async (req, res) => {
    try {
        const CareSiteId = req.query.care_site_id;
        if (!CareSiteId)
            return res.status(400).send("care site is required");

        // set time interval
        const START = new Date(req.query.start).setHours(0, 0, 0, 0);
        const END = new Date(req.query.end).setHours(23, 59, 59, 59);
        if(!START || !END)
            return res.status(400).send("time interval is missing");

        let result = await StatCareSite.getTotalVisitPatient(START, END, CareSiteId);
        if (!result.isSuccess)
            return res.status(400).send('Cannot List Total Visit.\n' + result.message);

        return res.status(200).send(result.data);
    }
    catch (error) {
        return res.status(400).json(error.name);
    }
};
